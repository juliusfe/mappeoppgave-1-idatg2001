package hospital;

import hospital.exception.RemoveException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class DepartmentTest {

    Department testDepartment;
    Employee employee;
    Employee employee2;
    Patient patient;
    Patient patient2;



    @BeforeEach
    void creatTestData(){
        testDepartment = new Department("testDepartment");
        employee = new Employee("Jon", "Lee", "123456");
        employee2 = new Employee("Bertine", "Smith", "13579");
        patient = new Patient("Bert", "Johnsrud", "654321");
        testDepartment.addEmployee(employee);
        testDepartment.addPatient(patient);
    }

    @Test
    void removeEmployeePositiveTest() {
        try {
            testDepartment.remove(employee);
        }
        catch (RemoveException e){
            e.printStackTrace();
        }
        Boolean testBoolean = (testDepartment.getEmployees().containsKey(employee.getPersonnummer()));
        assertFalse(testBoolean);
    }

    @Test
    void removePatientPositiveTest(){
        try {
            testDepartment.remove(patient);
        }
        catch (RemoveException e){
            e.printStackTrace();
        }
        Boolean testBoolean = (testDepartment.getPatients().containsKey(patient.getPersonnummer()));
        assertFalse(testBoolean);

    }

    @Test
    void removeEmployeeNegativeTest() {
            assertThrows(RemoveException.class, ()-> {testDepartment.remove(employee2);});
        }

    @Test
    void removePatientNegativeTest() {
        assertThrows(RemoveException.class, ()-> {testDepartment.remove(patient2);});
    }
}
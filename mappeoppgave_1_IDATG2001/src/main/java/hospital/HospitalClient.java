package hospital;

import hospital.exception.RemoveException;

public class HospitalClient {


    public static void main(String[] args) {
        //Adding hospital.
        Hospital hospital = new Hospital("Hospital");
        //Filling hospital with data.
        HospitalTestData.fillRegisterWithTestData(hospital);
        //Finding department object.
        Department department = (Department) hospital.getDepartments().get(0);
        //Printing initial number of employees in department.
        System.out.println("Before removal: " + department.getEmployees().size());
        //Finding person in department.
        Person person = (Person) department.getEmployees().get("1111");
        //Removing person from department.
        try {
            department.remove(person);
            //Printing new number of employees in department.
        } catch (RemoveException e) {
            e.printStackTrace();
        }
        System.out.println("After removal: " + department.getEmployees().size());

        //Making a new patioent to test with
        Patient patient = new Patient("Harrald", "Hårfagre", "6666");
        //Attempting to remove patient that is not registered.
        System.out.println("Attempting to remove fake patient:");
        try {
            department.remove(person);
            //Printing new number of employees in department.
        } catch (RemoveException e) {
            e.printStackTrace();
        }
    }
}

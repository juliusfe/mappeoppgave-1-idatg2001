package hospital;

import hospital.exception.RemoveException;

import java.util.HashMap;
import java.util.Objects;

/**
 * Class that represents one department in a hospital.
 */
public class Department {

    private String departmentName;
    private HashMap<String, Employee> employees;
    private HashMap<String, Patient> patients;

    /**
     * Constructor for department. Also initializes list for employess and patients.
     * @param name
     */
    public Department(String name) {
        this.departmentName = name;
        employees = new HashMap<>();
        patients = new HashMap<>();
    }

    /**
     * Gets the name of det department.
     * @return the name of the department.
     */
    public String getName() {
        return departmentName;
    }

    /**
     * Sets the name of the department.
     * @param name
     */
    public void setName(String name) {
        this.departmentName = name;
    }

    /**
     * Adds an employee to the list of employees in the department.
     * @param employee
     */
    public void addEmployee(Employee employee){
        employees.put(employee.getPersonnummer(), employee);
    }

    /**
     * Gets a list of all employees in the department.
     * @return a list of all employees in the department.
     */
    public HashMap getEmployees(){
        return employees;
    }

    /**
     * Adds a patient to the list of patients in the department.
     * @param patient
     */
    public void addPatient(Patient patient){
        patients.put(patient.getPersonnummer(),patient);
    }

    /**
     * Gets a list of all the patients in the department.
     * @return a list of all the patients in the department.
     */
    public HashMap getPatients(){
        return patients;
    }

    public void remove(Person person) throws RemoveException {

            if(person instanceof Employee){
                if (employees.containsKey(person.getPersonnummer()))
                    employees.remove(person.getPersonnummer());
                else throw new RemoveException("person does not match any persons in registry.");
            }
            else if (person instanceof Patient){
                if (patients.containsKey(person.getPersonnummer()))
                    patients.remove(person.getPersonnummer());
                else throw new RemoveException("person does not match any persons in registry.");
            }
            else throw new RemoveException("Object not recognized as a patient or employee.");
    }

    @Override
    public String toString() {
        return "Department{" +
                "name='" + departmentName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }
}

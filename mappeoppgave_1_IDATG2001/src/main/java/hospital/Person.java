package hospital;

/**
 * Abstract class for person-objects. will be used to created multiple person sub-classes with some shared functionality.
 */
public abstract class Person {

    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * Instantiates a new Person.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        setFirstName(firstName);
        setLastName(lastName);
        setPersonnummer(socialSecurityNumber);
    }

    /**
     * Gets the first name of a person.
     *
     * @return first name of a person.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the first name of a person.
     *
     * @param firstName the first name
     */
    public void setFirstName(String firstName) {
        if (firstName == null || firstName.isBlank()){
            throw (new IllegalArgumentException("First name can't be null or blank."));
        }
        else this.firstName=firstName;
    }

    /**
     * Gets the last name of a person.
     *
     * @return last name of a person.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the last name of a person.
     *
     * @param lastName the last name
     */
    public void setLastName(String lastName) {
        if (lastName == null || lastName.isBlank()){
            throw (new IllegalArgumentException("Last name can't be null or blank."));
        }
        else this.lastName = lastName;
    }

    /**
     * Gets the full name of a person as a string.
     *
     * @return full name.
     */
    public String getFullNavn() {
        return (this.firstName + " " + this.lastName);

    }

    /**
     * Gets the social security number of a person.
     *
     * @return the social security number of a person.
     */
    public String getPersonnummer() {
        return socialSecurityNumber;
    }

    /**
     * sets the social security number of a person.
     *
     * @param socialSecurityNumber the social security number
     */
    public void setPersonnummer(String socialSecurityNumber) {
        if (socialSecurityNumber == null || socialSecurityNumber.isBlank()){
            throw  (new IllegalArgumentException("Social security number can't be null or blank."));
        }
        else this.socialSecurityNumber = socialSecurityNumber;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", socialSecurityNumber='" + socialSecurityNumber + '\'' +
                '}';
    }
}

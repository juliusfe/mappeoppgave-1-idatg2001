package hospital.exception;

public class RemoveException extends Exception{

    private static final Long serialVersionUID = 1L;

    public RemoveException(String message){
        super(message);
    }
}

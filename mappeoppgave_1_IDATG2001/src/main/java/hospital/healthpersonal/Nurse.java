package hospital.healthpersonal;

import hospital.Employee;

public class Nurse extends Employee {


    /**
     * Constructor to make on Nurse object.
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Nurse{" +
                "firstName='" + this.getFirstName() + '\'' +
                ", lastName='" + this.getLastName() + '\'' +
                ", socialSecurityNumber='" + this.getPersonnummer() + '\'' +
                '}';
    }
}

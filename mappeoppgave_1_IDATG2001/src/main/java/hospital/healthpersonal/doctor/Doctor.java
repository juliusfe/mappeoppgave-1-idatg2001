package hospital.healthpersonal.doctor;

import hospital.Employee;
import hospital.Patient;

public abstract class Doctor extends Employee {


    public Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Method to set the diagnosis of a patient.
     * @param patient
     * @param diagnosis
     */
    abstract void setDiagnosis(Patient patient, String diagnosis);
}

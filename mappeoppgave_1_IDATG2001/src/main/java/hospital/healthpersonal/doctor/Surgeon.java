package hospital.healthpersonal.doctor;

import hospital.Patient;

public class Surgeon extends Doctor {


    /**
     * Constructor to make one Surgeon object.
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    void setDiagnosis(Patient patient, String diagnosis) {
        Patient currentPatient = patient;
        patient.setDiagnosis(diagnosis);
    }

}

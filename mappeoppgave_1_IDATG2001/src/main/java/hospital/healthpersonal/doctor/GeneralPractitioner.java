package hospital.healthpersonal.doctor;

import hospital.Patient;

public class GeneralPractitioner extends Doctor {

    /**
     * Constructor to make on Doctor object.
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    void setDiagnosis(Patient patient, String diagnosis) {
        Patient currentPatient = patient;
        patient.setDiagnosis(diagnosis);
    }

}

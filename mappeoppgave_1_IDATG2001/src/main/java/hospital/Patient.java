package hospital;

/**
 * Class made to represent a patient. extedns Person class to get main functionality and ads spesifics for a patient.
 */
public class Patient extends Person implements Diagnosable {

    private String diagnosis;

    /**
     * Constructor for a patient object. Does not initialize the diagnosis field as this will be set later.
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * gets the diagnosis of a patient.
     * @return the diagnosis set on the patient.
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    @Override
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "diagnosis='" + diagnosis + '\'' +
                "firstName='" + this.getFirstName() + '\'' +
                ", lastName='" + this.getLastName() + '\'' +
                ", socialSecurityNumber='" + this.getPersonnummer() + '\'' +
                '}';
    }
}

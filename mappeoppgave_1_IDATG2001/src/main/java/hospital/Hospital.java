package hospital;

import java.util.ArrayList;

public class Hospital {

    private final String hospitalName;
    private ArrayList<Department> departments;

    /**
     * Constructor for the hospital object.
     * @param hospitalName
     */
    public Hospital(String hospitalName) {
            this.hospitalName = hospitalName;
            departments = new ArrayList<>();
    }

    /**
     * Gets the hospital name.
     * @return the hospital name.
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * adds department objects to the hospital.
     * @param department
     */
    public void addDepartment(Department department){
        departments.add(department);
    }

    /**
     * retuns a list of all the departments in the hospital.
     * @return
     */
    public ArrayList getDepartments(){
        return departments;
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                '}';
    }
}

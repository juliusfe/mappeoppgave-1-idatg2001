package hospital;

import hospital.healthpersonal.Nurse;
import hospital.healthpersonal.doctor.GeneralPractitioner;
import hospital.healthpersonal.doctor.Surgeon;

public final class HospitalTestData {

   private HospitalTestData() {
        // not called
    }
    /**
     * @param hospital
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {
        // Add some departments
        Department emergency = new Department("Akutten");
        emergency.getEmployees().put("1111",(new Employee("Odd Even", "Primtallet", "1111")));
        emergency.getEmployees().put("1112",(new Employee("Huppasahn", "DelFinito", "1112")));
        emergency.getEmployees().put("1113", (new Employee("Rigmor", "Mortis", "1113")));
        emergency.getEmployees().put("1114", (new GeneralPractitioner("Inco", "Gnito", "1114")));
        emergency.getEmployees().put("1115", (new Surgeon("Inco", "Gnito", "1115")));
        emergency.getEmployees().put("1116", (new Nurse("Nina", "Teknologi", "1116")));
        emergency.getEmployees().put("1117", (new Nurse("Ove", "Ralt", "1117")));
        emergency.getPatients().put("1118", (new Patient("Inga", "Lykke", "1118")));
        emergency.getPatients().put("1119", (new Patient("Ulrik", "Smål", "1119")));
        hospital.getDepartments().add(emergency);
        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.getEmployees().put("1120", (new Employee("Salti", "Kaffen", "1120")));
        childrenPolyclinic.getEmployees().put("1127", (new Employee("Nidel V.", "Elvefølger", "1127")));
        childrenPolyclinic.getEmployees().put("1121", (new Employee("Anton", "Nym", "1121")));
        childrenPolyclinic.getEmployees().put("1128", (new GeneralPractitioner("Gene", "Sis", "1128")));
        childrenPolyclinic.getEmployees().put("1122", (new Surgeon("Nanna", "Na", "1122")));
        childrenPolyclinic.getEmployees().put("1123", (new Nurse("Nora", "Toriet", "1123")));
        childrenPolyclinic.getPatients().put("1124", (new Patient("Hans", "Omvar", "1124")));
        childrenPolyclinic.getPatients().put("1125", (new Patient("Laila", "La", "1125")));
        childrenPolyclinic.getPatients().put("1126", (new Patient("Jøran", "Drebli", "1126")));
        hospital.getDepartments().add(childrenPolyclinic);
    }
    }

package hospital;

/**
 * Interface made to hold setDiagnosis method that will be used in Patient class.
 */
public interface Diagnosable {

    /**
     * Sets the diagnosis of a patient.
     * @param diagnosis
     */
    void setDiagnosis(String diagnosis);

}
